package com.example.merchantService.configuration;


import com.example.merchantService.model.ProductModel;
import com.example.merchantService.model.ProductRequestModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

@FeignClient(name = "product-service", url = "http://localhost:8081", path = "/product-service")
public interface ProductServiceFeign {
    @PostMapping(
            value = "/v1.0/product/checkProduct")
    String checkProduct(@RequestBody ProductRequestModel productRequest);

    @PostMapping(
            value = "/v1.0/product/addProduct")
    ResponseEntity<String> addProduct(@RequestBody ProductModel productModel);

}

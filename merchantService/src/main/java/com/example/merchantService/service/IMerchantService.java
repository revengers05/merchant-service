package com.example.merchantService.service;

import com.example.merchantService.model.MerchantModel;
import com.example.merchantService.model.MerchantProductModel;
import com.example.merchantService.model.ProductModel;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface IMerchantService {
    void deleteProductByProductId(String productId, Integer merchantId);

    void updateProductStockByCount(String productId, Integer merchantId, Integer count);

    void setProductStock(String productId, Integer merchantId, Integer newStock);
    String addNewProduct(MerchantModel product);

    List<ProductModel> getAllProductsOfMerchant(Integer merchantId);

    List<MerchantProductModel> getAllMerchantsWithPid(@RequestParam String productId);
}

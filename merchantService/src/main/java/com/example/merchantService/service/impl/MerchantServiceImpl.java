package com.example.merchantService.service.impl;

import com.example.merchantService.configuration.ProductServiceFeign;
import com.example.merchantService.entity.MerchantEntity;
import com.example.merchantService.model.*;
import com.example.merchantService.repository.IMerchantRepository;
import com.example.merchantService.service.IMerchantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Slf4j
@Service
public class MerchantServiceImpl implements IMerchantService {
    @Autowired
    IMerchantRepository merchantRepository;
    @Autowired
    ProductServiceFeign productServiceFeign;

    public void deleteProductByProductId(String productId, Integer merchantId) {
        MerchantEntity response = merchantRepository.findByMerchantIdAndProductId(merchantId, productId);
        if (Objects.nonNull(response)) {
            log.info("Merchant have product to delete: {}", response);
            merchantRepository.deleteByProductIdAndMerchantId(productId, merchantId);
        }
        //Feign call to delete stock in the repository.
    }

    public void updateProductStockByCount(String productId, Integer merchantId, Integer count) {
        log.info("Got call to update product Stock by: {} with productId: {}, merchantId: {}", count, productId, merchantId);
        MerchantEntity merchantEntity = merchantRepository.findByMerchantIdAndProductId(merchantId, productId);
        Integer currentStock = merchantEntity.getStock();
        merchantRepository.updateStockByMerchantIdAndProductId(currentStock + count, merchantId, productId);
        String url = "http://localhost:8081/product-service/v1.0/product/updateStock";
        RestTemplate restTemplate = new RestTemplate();
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
                .queryParam("productId", productId)
                .queryParam("count", count);
        ResponseEntity<Void> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, null, Void.class);
    }

    public void setProductStock(String productId, Integer merchantId, Integer newStock) {
        log.info("Getting all the products of merchant with id: {}", merchantId);
        merchantRepository.updateStockByMerchantIdAndProductId(newStock, merchantId, productId);
        //Update the inventory stock.
//        String url = "http://localhost:8081/product-service/v1.0/product/updateStock";
////        log.info("Url is: {}", url);
//        RestTemplate restTemplate = new RestTemplate();
//        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url)
//                .queryParam("productId", productId)
//                .queryParam("count", newStock);
//
//        ResponseEntity<Void> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, null, Void.class);
    }

    public String addNewProduct(MerchantModel merchantModel) {
        log.info("Got call to add new product: model: {}, merchantId: {}", merchantModel, merchantModel.getMerchantId());
        //Bring product id from mongo feign call;
        ProductRequestModel productRequestModel = new ProductRequestModel();
        productRequestModel.setProductName(merchantModel.getProductName());
        productRequestModel.setColor(merchantModel.getColor());
        productRequestModel.setRam(merchantModel.getRam());
        log.info("Calling product service");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<ProductRequestModel> requestEntity = new HttpEntity<>(productRequestModel, headers);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("http://localhost:8081/product-service/v1.0/product/checkProduct", requestEntity, String.class);
        log.info("response entity from product service is: {}", responseEntity);
        String productId = responseEntity.getBody();
        System.out.println("Response from the server: " + productId);
        if (!StringUtils.isEmpty(productId)) {
            log.info("Got product id: {} of the product with name: {}", productId, merchantModel.getProductName());
            MerchantEntity merchantEntity = new MerchantEntity();
            merchantEntity.setMerchantId(merchantModel.getMerchantId());
            merchantEntity.setProductId(productId);
            merchantEntity.setPrice(merchantModel.getPrice());
            merchantEntity.setStock(merchantModel.getStock());
            merchantEntity.setDescription(merchantModel.getDescription());
            merchantRepository.save(merchantEntity);
        } else {
            log.info("No product available, going to add this as a new product");
            ProductModel productModel = new ProductModel();
            productModel.setProductName(merchantModel.getProductName());
            productModel.setColor(merchantModel.getColor());
            productModel.setRam(merchantModel.getRam());
            productModel.setUsp(merchantModel.getUsp());
            productModel.setImageUrl(merchantModel.getImageUrl());
            productModel.setTotalStock(merchantModel.getStock());
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<ProductModel> requestEnt = new HttpEntity<>(productModel, headers);
            ResponseEntity<String> responseEnt = restTemplate.postForEntity("http://localhost:8081/product-service/v1.0/product/addProduct", requestEnt, String.class);
            String pId = responseEnt.getBody();
            if (!pId.isEmpty()) {
                log.info("Product id for newly created product is: {}", pId);
                MerchantEntity merchantEntity = new MerchantEntity();
                merchantEntity.setMerchantId(merchantModel.getMerchantId());
                merchantEntity.setProductId(pId);
                merchantEntity.setPrice(merchantModel.getPrice());
                merchantEntity.setStock(merchantModel.getStock());
                merchantEntity.setDescription(merchantModel.getDescription());
                merchantRepository.save(merchantEntity);
            }
            return pId;
        }
        return "";
    }

    public List<ProductModel> getAllProductsOfMerchant(Integer merchantId) {
        log.info("Getting product ids of products available for merchants: {}", merchantId);
        List<ProductModel> merchantProducts = new ArrayList<>();
        List<MerchantEntity> products = merchantRepository.findAllByMerchantId(merchantId);
        List<ProductIdAndMerchantPrice> merchantProductIdList = new ArrayList<>();

        log.info("Merchants products in mariaDb is: {}", products);
        if (!products.isEmpty()) {
            for (MerchantEntity product : products) {
                ProductIdAndMerchantPrice productIdAndMerchantPrice = new ProductIdAndMerchantPrice();
                productIdAndMerchantPrice.setProductId(product.getProductId());
                productIdAndMerchantPrice.setPrice(product.getPrice());
                productIdAndMerchantPrice.setTotalStock(product.getStock());
                merchantProductIdList.add(productIdAndMerchantPrice);
            }
            log.info("Calling product service to get products infos");
            RestTemplate restTemplate = new RestTemplate();
            for (ProductIdAndMerchantPrice product : merchantProductIdList) {
                String productId = product.getProductId();
                log.info("Product price is: {}", product.getPrice());
                ProductModel model = new ProductModel();
                String url = "http://localhost:8081/product-service/v1.0/product/getProductById?productId={productId}";
                ResponseEntity<ProductModel> response = restTemplate.getForEntity(url, ProductModel.class, productId);
                log.info("Product details is: {}", response);
                log.info("{}", response.getBody());
                model.setPrice(product.getPrice());
                model.setTotalStock(product.getTotalStock());
                model.setId(response.getBody().getId());
                model.setColor(response.getBody().getColor());
                model.setImageUrl(response.getBody().getImageUrl());
                model.setUsp(response.getBody().getUsp());
                model.setProductName(response.getBody().getProductName());
                model.setRam(response.getBody().getRam());
                merchantProducts.add(model);
            }
            return merchantProducts;
        }
        return merchantProducts;
    }

    public List<MerchantProductModel> getAllMerchantsWithPid(String productId) {
        List<MerchantEntity> merchants = new ArrayList<>();
        List<MerchantProductModel> merchantsList = new ArrayList<>();
        merchants = merchantRepository.findAllByProductId(productId);
        log.info("Merchants selling products with Id: {} are: {}", productId, merchants);
        if (Objects.nonNull(merchants)) {
            for (MerchantEntity merchant : merchants) {
                MerchantProductModel temp = new MerchantProductModel();
                temp.setMerchantId(merchant.getMerchantId());
                temp.setProductId(merchant.getProductId());
                temp.setPrice(merchant.getPrice());
                temp.setStock(merchant.getStock());
                temp.setDescription(merchant.getDescription());
                merchantsList.add(temp);
            }
        }
        return merchantsList;
    }

}

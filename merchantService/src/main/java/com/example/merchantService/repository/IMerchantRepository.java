package com.example.merchantService.repository;

import com.example.merchantService.entity.MerchantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface IMerchantRepository extends JpaRepository<MerchantEntity, Integer> {
    @Modifying
    @Transactional
    @Query("UPDATE MerchantEntity e SET e.stock = :newStock WHERE e.merchantId = :merchantId AND e.productId = :productId")
    void updateStockByMerchantIdAndProductId(Integer newStock, Integer merchantId, String productId);

    List<MerchantEntity> findAllByMerchantId(Integer merchantId);

    MerchantEntity findByMerchantIdAndProductId(Integer merchantId, String productId);

    @Modifying
    @Transactional
    @Query("DELETE FROM MerchantEntity e WHERE e.merchantId = :merchantId AND e.productId = :productId")
    void deleteByProductIdAndMerchantId(String productId, Integer merchantId);

    List<MerchantEntity> findAllByProductId(String productId);

}

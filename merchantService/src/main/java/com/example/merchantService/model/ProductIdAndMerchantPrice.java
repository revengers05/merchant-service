package com.example.merchantService.model;

public class ProductIdAndMerchantPrice {
    String productId;
    Double price;

    Integer totalStock;

    public Integer getTotalStock() {
        return totalStock;
    }

    public void setTotalStock(Integer totalStock) {
        this.totalStock = totalStock;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductIdAndMerchantPrice{" +
                "productId='" + productId + '\'' +
                ", price=" + price +
                '}';
    }
}

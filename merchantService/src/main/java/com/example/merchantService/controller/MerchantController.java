package com.example.merchantService.controller;

import com.example.merchantService.model.MerchantModel;
import com.example.merchantService.model.MerchantProductModel;
import com.example.merchantService.model.ProductModel;
import com.example.merchantService.service.IMerchantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/v1.0/merchant-service")
public class MerchantController {
    @Autowired
    IMerchantService merchantService;
    @DeleteMapping(value = "/deleteProduct")
    public void deleteProductById(@RequestParam Integer merchantId, @RequestParam String productId){
        merchantService.deleteProductByProductId(productId, merchantId);
        log.info("Got call to delete product with id: {} for merchant id: {}", productId, merchantId);
    }

    @PostMapping(value = "/addProduct")
    public String addProduct(@RequestBody MerchantModel product){
        log.info("Got Call to add a new product: {}", product);
        merchantService.addNewProduct(product);
        return "";
    }

    @PutMapping(value = "/updateProduct/count")
    public void updateProductStockByCount(@RequestParam String productId, @RequestParam Integer merchantId, @RequestParam Integer count){
        log.info("Got call to update product Stock by: {} with productId: {}, merchantId: {}", count, productId, merchantId );
        merchantService.updateProductStockByCount(productId, merchantId, count);
    }

    @PutMapping(value = "/updateProductStock")
    public void setProductStock(@RequestParam String productId, @RequestParam Integer merchantId, @RequestParam Integer newStock){
        log.info("Got call to update stock for merchantId: {}, product id: {}, by new stock amount: {}", merchantId, productId, newStock);
        merchantService.setProductStock(productId, merchantId, newStock);
    }

    @GetMapping(value = "merchant/products")
    public List<ProductModel> getAllProductsOfMerchant(@RequestParam Integer merchantId){
        log.info("Got call to get all products od merchant with id: {}", merchantId);
        return merchantService.getAllProductsOfMerchant(merchantId);
    }
    @GetMapping(value = "product/merchants")
    public List<MerchantProductModel> getAllMerchantsWithPid(@RequestParam String productId){
        log.info("Got call to get all the merchants selling the product with is: {}", productId);
        return merchantService.getAllMerchantsWithPid(productId);
    }

}
